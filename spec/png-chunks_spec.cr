require "./spec_helper"

describe PNGChunks::Reader do
  it "reads PNG chunks" do
    file = File.new("./spec/fixtures/12x34.png")

    chunks = PNGChunks::Reader.new(file).to_a

    chunks[0].type.should eq("IHDR")
    chunks[0].data.should be_a(Bytes)
    chunks[0].data.to_a.should eq([
      # Width (12)
      0x00,
      0x00,
      0x00,
      0x0c,
      # Height (34)
      0x00,
      0x00,
      0x00,
      0x22,
      # Bit depth
      0x08,
      # Colour type
      0x02,
      # Compression, filter, and interlace method
      0x00,
      0x00,
      0x00,
    ])

    chunks[1].type.should eq("IDAT")
    chunks[1].data.should_not be_empty

    chunks[2].type.should eq("IEND")
    chunks[2].data.should be_empty
  end
end
