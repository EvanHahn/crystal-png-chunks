# Crystal PNG chunks

A Crystal shard offering low-level access to PNG data.

## Installation

1. Add the dependency to your `shard.yml`:

   ```yaml
   dependencies:
     png-chunks:
       gitlab: EvanHahn/crystal-png-chunks
   ```

2. Run `shards install`.

## Usage

```crystal
require "png-chunks"

file = File.new("12x34.png")

reader = PNGChunk::Reader.new(file)

puts reader.first.type # => "IHDR"
puts reader.first.data # => Bytes[0x00, 0x00, 0x00, 0x0c, ...]
```
