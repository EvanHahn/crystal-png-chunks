require "digest/crc32"

# The `PNGChunks` module provides low-level access to reading PNG data.
#
# It doesn't deal with pixel data, compression, or actual image decoding. You may wish to use [stumpy_png](https://github.com/stumpycr/stumpy_png) for higher-level operations.
#
# ### Reading example
#
# ```
# file = File.new("12x34.png")
#
# reader = PNGChunks::Reader.new(file)
#
# puts reader.first.type # => "IHDR"
# puts reader.first.data # => Bytes[0x00, 0x00, 0x00, 0x0c, ...]
# ```
module PNGChunks
  VERSION = "0.1.0"

  # The eight bytes that should be at the start of every PNG.
  private PNG_SIGNATURE = Bytes.new(8)
  PNG_SIGNATURE[0] = 0x89
  PNG_SIGNATURE[1] = 0x50
  PNG_SIGNATURE[2] = 0x4e
  PNG_SIGNATURE[3] = 0x47
  PNG_SIGNATURE[4] = 0x0d
  PNG_SIGNATURE[5] = 0x0a
  PNG_SIGNATURE[6] = 0x1a
  PNG_SIGNATURE[7] = 0x0a

  # This exception is thrown when the first eight bytes of data are not the PNG signature, indicating that the PNG is invalid.
  class InvalidPngSignatureException < Exception
    def initialize
      super("Invalid PNG signature. Is this a PNG?")
    end
  end

  # This exception is thrown when, for some reason, the provided `IO` doesn't produce enough bytes.
  class PrematureEndException < Exception
    def initialize
      super("PNG data ended prematurely")
    end
  end

  # This exception is thrown when a chunk's checksum is wrong.
  class InvalidChecksumException < Exception
    def initialize
      super("Invalid PNG checksum")
    end
  end

  # `PNGChunk` represents a single PNG chunk, which is effectively the pair of the type (such as `IHDR`) and the data.
  class PNGChunk
    # The chunk's type as `Bytes`. For example, `IHDR` is `Bytes[0x49, 0x48, 0x44, 0x52]`.
    getter type_bytes : Bytes

    # The chunk's data.
    getter data : Bytes

    # The chunk's CRC32 checksum, calculated from the chunk's type and data.
    getter crc : UInt32

    # Creates a new `PNGChunk` out of a type and data, both `Byte`s.
    #
    # ```
    # type_bytes = "IHDR".bytes
    # data = make_ihdr_bytes
    # chunk = PNGChunks::PNGChunk(type_bytes, data)
    # ```
    def initialize(@type_bytes, @data)
      type_checksum = Digest::CRC32.checksum(@type_bytes)
      @crc = Digest::CRC32.update(@data, type_checksum)
    end

    # The chunk's type as a string.
    def type
      String.new(@type_bytes)
    end

    # A boolean representing whether the chunk is valid.
    # A chunk is valid if its `type` is made up of ASCII letters (A-Z and a-z) and if its `data` will fit in a PNG's maximum chunk size.
    def valid? : Bool
      type_valid? && data_valid?
    end

    private def type_valid? : Bool
      @type_bytes.all? do |byte|
        is_capital_letter = byte >= 65 && byte <= 90
        is_lowercase_letter = byte >= 97 && byte <= 122
        is_capital_letter || is_lowercase_letter
      end
    end

    private def data_valid? : Bool
      @data.size <= 0xffffffff
    end
  end

  # A class to read PNG data. It's an `Enumerable` that produces `PNGChunks::PNGChunk`s.
  #
  # ```
  # file = File.new("12x34.png")
  #
  # reader = PNGChunks::Reader.new(file)
  #
  # puts reader.first.type # => "IHDR"
  # puts reader.first.data # => Bytes[0x00, 0x00, 0x00, 0x0c, ...]
  # ```
  class Reader
    include Enumerable(PNGChunk)

    def initialize(@io : IO)
    end

    def each
      first_eight_bytes = Bytes.new(8)
      @io.read(first_eight_bytes)
      raise InvalidPngSignatureException.new unless PNG_SIGNATURE == first_eight_bytes

      length_buffer = Bytes.new(4)

      while true
        length_bytes_read = @io.read(length_buffer)
        break if length_bytes_read == 0
        raise PrematureEndException.new unless length_bytes_read == 4

        type_buffer = Bytes.new(4)
        type_bytes_read = @io.read(type_buffer)
        # TODO: what if type is invalid?
        raise PrematureEndException.new unless type_bytes_read == 4

        data_length = IO::ByteFormat::BigEndian.decode(UInt32, length_buffer)
        # TODO: max length?
        data = Bytes.new(data_length)
        data_bytes_read = @io.read(data)
        raise PrematureEndException.new unless data_bytes_read == data_length

        chunk = PNGChunk.new(type_buffer, data)

        crc_buffer = Bytes.new(4)
        crc_bytes_read = @io.read(crc_buffer)
        raise PrematureEndException.new unless crc_bytes_read == 4
        expected_crc = IO::ByteFormat::BigEndian.decode(UInt32, crc_buffer)
        raise InvalidChecksumException.new unless expected_crc == chunk.crc

        yield chunk
      end
    end
  end
end
